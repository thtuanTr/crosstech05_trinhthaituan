import React from 'react';
import logo from './logo.svg';
import './App.css';
import TestState from './Hook/TestState';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <TestState></TestState>
      </header>
    </div>
  );
}

export default App;
