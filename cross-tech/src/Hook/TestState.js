import React, { useState } from 'react';
import TestState2 from './TestState2';

export default function TestState() {
  const [count, setCount] = useState(10);
  const handleCount = () => setCount(count + 1);
  return (
    <div>
      <TestState2 count={count} handleCount={handleCount} />
    </div>
  );
}
