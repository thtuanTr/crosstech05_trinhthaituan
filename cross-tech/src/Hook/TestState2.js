import React from 'react';

export default function TestState2(props) {
  return (
    <div>
      <p>You clicked {props.count} times</p>
      <button onClick={props.handleCount}>Click me</button>
    </div>
  );
}
